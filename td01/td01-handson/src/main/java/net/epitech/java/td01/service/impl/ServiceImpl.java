package net.epitech.java.td01.service.impl;

import java.util.Collection;
import java.util.HashSet;

import net.epitech.java.td01.model.Course;
import net.epitech.java.td01.model.Teacher;
import net.epitech.java.td01.service.contract.Service;

//TODO:this class should be unit tested
@org.springframework.stereotype.Service
public class ServiceImpl implements Service
{
	public ServiceImpl()
	{
		this.addTeacher("miramond", "miramo_m@epitech.eu", 1);
	}
	
	public Collection<Course> getCourses()
	{
		return this._courses;
	}

	public Collection<Teacher> getTeachers()
	{
		return this._teachers;
	}

	public void addCourse(Course c)
	{
		this._courses.add(c);
	}

	public void deleteCourse(Course c)
	{
		this._courses.remove(c);
	}

	public boolean addTeacher(String name, String mail, Integer id)
	{
		boolean exist = false;

		for (Teacher t : this._teachers)
		{
			if (t.getId().equals(id))
				exist = true;
		}
		if (!exist)
		{
			Teacher t = new Teacher();
			t.setId(id);
			t.setName(name);
			t.setMail(mail);
			this._teachers.add(t);			
			return (true);
		}
		return (false);
	}

	public boolean deleteTeacher(Integer id)
	{
		boolean exist = false;
		Collection<Teacher> teacherstodelete = new HashSet<Teacher>();

		for (Teacher t : this._teachers)
		{
			if (t.getId().equals(id))
			{
				teacherstodelete.add(t);
				exist = true;
			}
		}
		this._teachers.removeAll(teacherstodelete);
		return (exist ? true : false);
	}
	
	private Collection<Course>	_courses;
	private Collection<Teacher>	_teachers;

}
